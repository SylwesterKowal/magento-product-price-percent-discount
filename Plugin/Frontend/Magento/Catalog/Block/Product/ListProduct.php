<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\DisplayPercentDiscount\Plugin\Frontend\Magento\Catalog\Block\Product;

class ListProduct
{
    public function __construct(
        \Kowal\DisplayPercentDiscount\Helper\Data $dataHelper
    )
    {
        $this->dataHelper = $dataHelper;
    }


    public function afterGetProductPrice(
        \Magento\Catalog\Block\Product\ListProduct $subject,
        $result,
        $product
    )
    {
        $store_id = $product->getStoreId() ?? 0;
        $enable = $this->dataHelper->getWebsiteCfg("enable", $store_id);
        $label =  $this->dataHelper->getWebsiteCfg("label_value", $store_id);
        $kolor_tla_lista =  $this->dataHelper->getWebsiteCfg("kolor_tla_lista", $store_id);
        $kolor_czcionki_lista =  $this->dataHelper->getWebsiteCfg("kolor_czcionki_lista", $store_id);
        if (!$enable) return $result;

        $simplePrice = 0;
        $_savingPercent = 0;
        if ($product->getTypeId() == "simple") {
            $simplePrice = $product->getPrice();
        } else {
            $_children = $product->getTypeInstance()->getUsedProducts($product);
            foreach ($_children as $child) {
                $simplePrice = $child->getPrice();
                break;
            }
        }

        $_finalPrice = $product->getFinalPrice();
        $_price = $simplePrice;
        if ($_finalPrice < $_price && $product->getTypeId() == "simple") {
            $_savingPercent = 100 - round(($_finalPrice / $_price) * 100);
            $result .= '<div class="kowal-rabat-list"><span class="discount-label" style="background-color: '.$kolor_tla_lista.'; color:'.$kolor_czcionki_lista.';">' . $label . ' ' . $_savingPercent . '%' . '</span></div>';
        }
        return $result;
    }
}

