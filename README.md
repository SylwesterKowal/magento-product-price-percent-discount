# Mage2 Module Kowal DisplayPercentDiscount

    ``kowal/module-displaypercentdiscount``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Display Percent Discount

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_DisplayPercentDiscount`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-displaypercentdiscount`
 - enable the module by running `php bin/magento module:enable Kowal_DisplayPercentDiscount`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (product_price_percent_discount/settings/enable)


## Specifications

 - Plugin
	- afterGetProductPrice - Magento\Catalog\Block\Product\ListProduct > Kowal\DisplayPercentDiscount\Plugin\Frontend\Magento\Catalog\Block\Product\ListProduct


## Attributes



